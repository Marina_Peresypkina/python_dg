"""
URL main Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
	url(r'^', include('z2_a_b.urls')),
	url(r'^z2_a_b/', include('z2_a_b.urls')),
]
