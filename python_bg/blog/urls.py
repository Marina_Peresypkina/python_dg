"""
URL Configuration for blog
"""
from django.conf.urls import url
from blog import views


urlpatterns = [
	url(r'^$', views.HomeView.as_view())
]
