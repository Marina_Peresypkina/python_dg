from django.views.generic import TemplateView 
from blog.models import Book, Author
from blog.actions import get_books


class HomeView(TemplateView):
	template_name = 'blog/home.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		context['some_th'] = "s"
		context['authors'] = [
			author for author in Author.objects.all()
		]
		context['books'] = get_books()
		return context
