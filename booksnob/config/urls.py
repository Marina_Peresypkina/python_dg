"""booksnob URL Configuration
"""
from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
	url(r'^critic/', include('critic.urls')),
	url(r'^', include('reader.urls')),
	url(r'^djrestfraimw/', include('djrestfraimw.urls')),
	url(r'^docs/', include('rest_framework_swagger.urls')),
]
