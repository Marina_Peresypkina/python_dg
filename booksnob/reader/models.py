from django.db import models
from django.contrib.auth.models import User
from django .contrib.contenttypes.models import ContentType


# user = ContentType.objects.get(app_label="auth", model="user")
# user.model_class()


# class Permission(models.Model):
# 	content_type = models.ForeignKey(ContentType)
# 	object_id = models.PositiveIntegerField()
# 	content_object = generic.GenericForeignKey('content_type', 'object_id')

class AuthorManager(models.Manager):

	def get_author(self):
		q = super(AuthorManager, self).get_queryset(self)
		return q.filter(first_name = "marina")

class Author(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField()
	avatar = models.ImageField(upload_to='images/author_avatar', blank=True)
	objects = models.Manager()
	filter_author = AuthorManager

	def __unicode__(self):
		return '{0} {1}'.format(self.first_name, self.last_name)

class Book(models.Model):
	author = models.ForeignKey(Author)
	name = models.CharField(max_length=100)

	def __unicode__(self):
		return '{0} - {1}'.format(self.author.first_name, self.name)

class Reader(models.Model):
	name = models.CharField(max_length=30)
	book = models.ManyToManyField(Book)

	def __unicode__(self):
		return '{0} - {1}'.format(self.name, self.book)

class UserProfile(models.Model):
	user   = models.OneToOneField(User)
	avatar = models.ImageField()