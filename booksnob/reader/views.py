from django.views.generic import TemplateView, DetailView, ListView, View 
from reader.models import Book, Author
from reader.actions import get_books, get_authors
from reader.forms import AuthorForm, AuthorEdit
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.edit import FormView, UpdateView  
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.conf import settings
import pytz
from django.views.decorators.cache import cache_page
from django.core.cache import cache



class HomeView(TemplateView):
	template_name = 'reader/home.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		context['authors'] = get_authors()
		context['books'] = get_books()
		return context

def get_author_filter(request):
	#Author.filter_author.get_author()
	form = AuthorForm()
	return HttpResponse(form)

class AuthorView(FormView):
	template_name = 'reader/authorform.html'
	form_class = AuthorForm
	success_url = '/getallauthor/'

	def form_valid(self, form):
		self.object = form.save()		
		return super(AuthorView, self).form_valid(form)


class GetAuthorView(DetailView):
	#print cache_page
	model = Author
	template_name = 'reader/thanks.html'
	
	def get_context_data(self, **kwargs):
		context = super(GetAuthorView, self).get_context_data(**kwargs)
		context['books'] = get_books()
		return context


class GetAllAuthors(ListView):
	model = Author
	template_name = 'reader/authors.html'

	def post(self, request, *args, **kwargs):
		context = request.POST.get('text', '')
		author = Author.objects.filter(first_name__icontains=context)
		return render(request, 'reader/authors.html', {'author_list': author})

class Register(FormView):
	template_name = 'reader/registr.html'
	success_url = '/registr/'
	form_class = UserCreationForm

	def form_valid(self, form):
		self.object = form.save()
		return super(Register, self).form_valid(form)

class Avtorization(FormView):
	template_name = 'reader/avtoriz.html'
	success_url = '/avtoriz/'
	form_class = AuthenticationForm

	def form_valid(self, form):
		user = form.get_user()
		login(self.request, user)
		return super(Avtorization, self).form_valid(form)

class LogOut(View):
	def get(self, request, *args, **kwargs):
		logout(request)
		return HttpResponseRedirect('/avtoriz/')

class AuthorEdit(UpdateView):
	model = Author
	fields = '__all__'
	template_name = 'reader/author_update_form.html'
	success_url = '/getallauthor/'


def set_timezone(request):
	if request.method == 'POST':
		request.session['django_timezone'] = request.POST['timezone']
		return redirect('/')
	else:
		return render(request, 'reader/timezone.html', {'timezones': pytz.common_timezones})


allauthors = cache.get('allauthors')
print "MyCache:"+str(allauthors)