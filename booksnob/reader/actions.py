from reader.models import Book, Author

def get_books():
	return 	[book for book in Book.objects.all()]

def get_authors():
	return 	[author for author in Author.objects.all()]