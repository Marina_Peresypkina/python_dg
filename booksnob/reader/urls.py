from django.conf.urls import url
from reader import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.decorators.cache import cache_page

urlpatterns = [
	#url(r'^$', views.HomeView.as_view()),
	url(r'^$', views.GetAllAuthors.as_view(), name="allauthors"),
	url(r'^get_author/$', views.AuthorView.as_view(), name="authorform"),
	url(r'^get_author/(?P<pk>\d+)/$', cache_page(60*1)(views.GetAuthorView.as_view()), name="authorid"),
	url(r'^getallauthor/$', views.GetAllAuthors.as_view(), name="allauthors"),
	url(r'^registr/$', views.Register.as_view(), name='registr'),
	url(r'^avtoriz/$', views.Avtorization.as_view(), name='avtoriz'),
	url(r'^avtoriz/logout/$', views.LogOut.as_view(), name='logout'),
	url(r'^timezone/$', views.set_timezone, name="set_timezone"),
	url(r'^author_edit/(?P<pk>\d+)/$', views.AuthorEdit.as_view(), name="author_edit"),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()