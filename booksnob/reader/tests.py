from django.test import TestCase

from reader import actions
from reader import models

class ActionsTestCase(TestCase):
	def setUp(self):
		self.author = models.Author.objects.create(
			first_name='fn',
			last_name='ln',
			email='em'
		)
		self.book = models.Book.objects.create(
			author=self.author,
			name='book'
		)

	def test_get_authors(self):
		authors = actions.get_authors()
		self.assertEqual(authors, [self.author])

	def test_get_books(self):
		books = actions.get_books()
		self.assertEqual(books, [self.book])
