# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0002_reader'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reader',
            old_name='reader',
            new_name='book_name',
        ),
    ]
