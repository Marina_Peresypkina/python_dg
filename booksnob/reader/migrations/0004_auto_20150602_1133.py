# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0003_auto_20150602_1123'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reader',
            old_name='book_name',
            new_name='book',
        ),
    ]
