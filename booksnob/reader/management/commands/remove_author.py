from django.core.management.base import BaseCommand, CommandError
from reader.models import Author


class Command(BaseCommand):
	help = 'remove author'

	def add_arguments(self, author_id):
		author_id.add_argument('author_id', nargs='+', type=int)
		author_id.add_argument('--delete', 
			action='store_true',
			dest='delete',
			default=False,
			help='Delete author_id')

	def handle(self, *args, **kwargs):
		try:
			if kwargs['author_id']:
				a = Author.objects.get(pk = str(kwargs['author_id'][0]))
				a.delete()
		except Exception:
			raise CommandError('Author "%s" does not exist' % kwargs['author_id'][0])
