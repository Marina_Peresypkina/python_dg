from django import forms
from reader.models import Author, Book, Reader
#from config.settings import MEDIA_ROOT

"""
class ReviewForm(forms.Form):
	book_name = forms.ChoiceField(choices=[
		(book, book) for book in get_books()
	])
	text = forms.CharField(widget=forms.Textarea)

	def send_email(self):
		print('Book: {0}'.format(self.cleaned_data['book_name']))
		print('Message: {0}'.format(self.cleaned_data['text']))
"""
class AuthorForm(forms.ModelForm):
	class Meta:
		model = Author
		fields = '__all__'
		#fields = ['first_name', 'last_name', 'email']

class AuthorEdit(forms.ModelForm):
    class Meta:
        model = Author
        fields = '__all__'