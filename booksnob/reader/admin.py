from django.contrib import admin
from reader.models import Author, Book, Reader


class AuthorAdmin(admin.ModelAdmin):
	#list_display= ('first_name', 'last_name')
	list_display = ('first_name', 'last_name', 'email')
	search_fields = ['first_name',]
	list_filter = ['first_name',]

class ReadersAdmin(admin.ModelAdmin):
	filter_horizontal = ['book',]

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book)
admin.site.register(Reader)