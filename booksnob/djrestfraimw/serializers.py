from rest_framework import routers, serializers, viewsets
from reader.models import Author

class Serial_Author_class(serializers.ModelSerializer):
	class Meta:
		model = Author
		#fields = '__all__'
		fields = ['pk','first_name', 'last_name', 'email']	

