from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from reader.models import Author
from djrestfraimw.serializers import Serial_Author_class
# from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.generics import GenericAPIView
from rest_framework import permissions


class AuthorListApi(APIView):

	def get(self, request):
		a = Author.objects.all()
		rez = Serial_Author_class(a, many=True)
		return Response(rez.data)

class GetAuthorApi(APIView):

	def get(self, request, pk):
		#a = Author.objects.all()
		a = Author.objects.get(pk = pk)
		rez = Serial_Author_class(a, many=False)
		return Response(rez.data)

	def post(self, request, pk):
		a = Author.objects.get(pk = pk)
		rez = Serial_Author_class(a, data=request.data)
		if rez.is_valid():
			rez.save()
			return Response(rez.data)
		return Response(rez.errors, status=status.HTTP_400_BAD_REQUEST)	

	def delete(self, request, pk):
		a = Author.objects.get(pk = pk)
		a.delete()
		return Response("ok")

# class AuthorListApi(ListModelMixin, CreateModelMixin, GenericAPIView):

# 	"""Get list of authors"""

# 	queryset = Author.objects.all()
# 	serializer_class = Serial_Author_class
		
# 	def get(self, request, *args, **kwargs):
# 		return self.list(request, *args, **kwargs)
	
# 	def post(self, request, *args, **kwargs):
# 		""" Add an author"""

# 		return self.create(request, *args, **kwargs)

# class GetAuthorApi(RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin, GenericAPIView):
	
# 	""" Get author by id"""

# 	permission_classes = (permissions.IsAuthenticated,)
# 	queryset = Author.objects.all()
# 	serializer_class = Serial_Author_class
	
# 	def get(self, request, *args, **kwargs):
# 		return self.retrieve(request, *args, **kwargs)

# 	def put(self, request, *args, **kwargs):
# 		""" Edit author"""

# 		return self.update(request, *args, **kwargs)

# 	def delete(self, request, *args, **kwargs):
# 		""" Delete an author"""

# 		return self.destroy(request, *args, **kwargs)