from django.conf.urls import url
from django.conf import settings
from djrestfraimw import views

urlpatterns = [
    #url(r'^restfraimw/', include('djrestfraimw.urls', namespace='restfraimw')),
	url(r'^author_list_serial/$', views.AuthorListApi.as_view(), name="author_list_serial"),
	url(r'^author_list_serial/(?P<pk>\d+)/$', views.GetAuthorApi.as_view(), name="author_one"),

]
