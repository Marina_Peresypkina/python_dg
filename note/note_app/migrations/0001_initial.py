# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_app', '0005_auto_20150620_1705'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', models.ForeignKey(related_name='child', blank=True, to='note_app.Category', null=True)),
                ('user', models.ForeignKey(to='user_app.UserProfile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20, choices=[(b'black', b'#000000'), (b'red', b'#FF0000'), (b'green', b'#00FF00'), (b'blue', b'#0000FF'), (b'yellow', b'#FFFF00'), (b'cyan', b'#00FFFF'), (b'purple', b'#FF00FF'), (b'grey', b'#C0C0C0')])),
            ],
        ),
        migrations.CreateModel(
            name='Font',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, choices=[(b'Times New Roman', b'Times New Roman'), (b'Georgia', b'Georgia'), (b'Arial', b'Arial'), (b'Verdana', b'Verdana'), (b'Courier New', b'Courier New'), (b'Lucida Console', b'Lucida Console')])),
            ],
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('media', models.TextField()),
                ('is_link', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('permiss_all_users', models.BooleanField(default=False)),
                ('category', models.ForeignKey(to='note_app.Category')),
                ('color', models.ForeignKey(to='note_app.Color')),
                ('font', models.ForeignKey(to='note_app.Font')),
            ],
        ),
        migrations.CreateModel(
            name='Permission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('another_user', models.ForeignKey(to='user_app.UserProfile')),
                ('note', models.ForeignKey(to='note_app.Note')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='note',
            name='tag',
            field=models.ManyToManyField(to='note_app.Tag'),
        ),
        migrations.AddField(
            model_name='note',
            name='user',
            field=models.ForeignKey(to='user_app.UserProfile'),
        ),
        migrations.AddField(
            model_name='media',
            name='note',
            field=models.OneToOneField(to='note_app.Note'),
        ),
    ]
