# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('note_app', '0002_auto_20150620_2008'),
    ]

    operations = [
        migrations.AddField(
            model_name='color',
            name='color_code',
            field=models.CharField(default=0, max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='color',
            name='name',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='font',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
