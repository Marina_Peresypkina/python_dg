# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('note_app', '0007_auto_20150622_0843'),
    ]

    operations = [
        migrations.AlterField(
            model_name='media',
            name='media',
            field=models.FileField(upload_to=b'diff_media', blank=True),
        ),
        migrations.AlterField(
            model_name='media',
            name='note',
            field=models.ForeignKey(to='note_app.Note'),
        ),
    ]
