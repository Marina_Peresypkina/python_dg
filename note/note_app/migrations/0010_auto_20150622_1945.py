# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('note_app', '0009_remove_media_is_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='media',
            name='file_type',
            field=models.CharField(default=0, max_length=10, choices=[(b'f', b'file'), (b'i', b'image'), (b'v', b'video')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='media',
            name='note',
            field=models.ForeignKey(related_name='medias', to='note_app.Note'),
        ),
    ]
