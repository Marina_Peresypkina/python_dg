# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('note_app', '0006_auto_20150622_0842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='permission',
            name='another_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='permission',
            name='note',
            field=models.ForeignKey(to='note_app.Note'),
        ),
    ]
