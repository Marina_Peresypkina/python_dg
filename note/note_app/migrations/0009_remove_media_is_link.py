# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('note_app', '0008_auto_20150622_1824'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='media',
            name='is_link',
        ),
    ]
