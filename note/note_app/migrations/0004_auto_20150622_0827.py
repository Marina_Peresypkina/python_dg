# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('note_app', '0003_auto_20150620_2017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='permission',
            name='another_user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='permission',
            name='note',
            field=models.OneToOneField(to='note_app.Note'),
        ),
    ]
