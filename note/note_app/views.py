from django.shortcuts import render, redirect, render_to_response
from rest_framework.views import APIView
from rest_framework.response import Response
from note_app.models import Category, Font, Color, Tag, Note, Media, Permission
from django.views.generic import TemplateView, DetailView, ListView, View 
from note_app.forms import AddNoteForm, EditNoteForm, AddTagForm, AddCategoryForm, AddMediaForm, AddPermissionForm
from django.views.generic.edit import FormView, UpdateView 
from django.views.generic import UpdateView
from django.contrib.auth import authenticate, login
from django.shortcuts import get_object_or_404



class NoteList(TemplateView):
	"""Get list of available for user notes on template"""

	template_name = 'note_app/notes_list.html'

class MyNoteList(TemplateView):
	"""Get list of user notes on template"""

	template_name = 'note_app/my_notes.html'


class MyNote(TemplateView):
	"""Get note by id on template"""

	template_name = 'note_app/note.html'

class AddNoteView(FormView):
	"""Add new note"""

	template_name = 'note_app/note_addform.html'
	form_class = AddNoteForm
	success_url = '/mynotes/'

	def form_valid(self, form): 
		form.instance.user = self.request.user
		self.object = form.save()   
		return super(AddNoteView, self).form_valid(form)

class EditNoteView(UpdateView):
	"""Edit new note"""

	model = Note
	template_name = 'note_app/note_editform.html'
	form_class = EditNoteForm
	success_url = '/mynote_list/'
	def form_valid(self, form):
		print "fgdd"
		self.object = form.save(commit=False)
		self.object.save()
		form.save_m2m()
		return redirect('mynote_list')

class AddTagView(FormView):
	"""Add new Tag"""

	template_name = 'note_app/tag_addform.html'
	form_class = AddTagForm
	success_url = '/add_note/'
	def form_valid(self, form):
		self.object = form.save()
		return super(AddTagView, self).form_valid(form) 


class AddCategoryView(FormView):
	"""Get note by id on template"""

	template_name = 'note_app/category_addform.html'
	form_class = AddCategoryForm
	success_url = '/add_note/'

	def form_valid(self, form):
		form.instance.user = self.request.user
		self.object = form.save()
		return super(AddCategoryView, self).form_valid(form) 


class AddMediaView(FormView):
	"""Add Media"""

	template_name = 'note_app/media_addform.html'
	form_class = AddMediaForm
	success_url = '/add_note/'

	def form_valid(self, form):
		form.instance.user = self.request.user
		self.object = form.save()
		return super(AddMediaView, self).form_valid(form) 


class AddPermissionView(FormView):
	"""Add user permission see note"""

	template_name = 'note_app/permission_addform.html'
	form_class = AddPermissionForm
	success_url = '/add_note/'

	def form_valid(self, form):
		self.object = form.save()
		return super(AddPermissionView, self).form_valid(form)