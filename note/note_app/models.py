# -*- coding: utf-8 -*-
from django.db import models
from user_app.models import UserProfile
from mptt.models import MPTTModel
import mptt
from django .contrib.contenttypes.models import ContentType

class Category(models.Model):
	user = models.ForeignKey(UserProfile)
	name = models.CharField(max_length=100)
	parent = models.ForeignKey('self', blank=True, null=True, related_name='child')

	def __unicode__(self):
		return self.name

mptt.register(Category,)


class Font(models.Model):

	name = models.CharField(max_length=100) 
	
	def __unicode__(self):		
		return self.name

class Color(models.Model):

	name = models.CharField(max_length=20)
	color_code = models.CharField(max_length=10)
	
	def __unicode__(self):		
		return self.name

class Tag(models.Model):

	name = models.CharField(max_length=100)

	def __unicode__(self):		
		return self.name


class Note(models.Model):

	user = models.ForeignKey(UserProfile)
	title = models.CharField(max_length=120)
	text = models.TextField()
	category = models.ForeignKey(Category)
	color = models.ForeignKey(Color)
	font = models.ForeignKey(Font)
	tag = models.ManyToManyField(Tag)
	permiss_all_users = models.BooleanField(default=False)

	def __unicode__(self):		
		return '{0} - {1}, {2}'.format(self.pk, self.title, self.user.username)

	def user_name(self):		
		return self.user.username

	def color_code(self):		
		return self.color.color_code

	def font_name(self):		
		return self.font.name

	def category_name(self):	
		return self.category.get_family().values()

class Media(models.Model):
	TYPE_LIST = (
		('f', 'file'),
		('i', 'image'),
		('v', 'video'),
	)
	note = models.ForeignKey(Note, related_name='medias')
	user = models.ForeignKey(UserProfile)
	media = models.FileField(upload_to='diff_media', blank=True)
	file_type = models.CharField(max_length=10, choices=TYPE_LIST)

	def __unicode__(self):		
		return '%s: %s' % (self.note, self.media)


class Permission(models.Model):
	note = models.ForeignKey(Note)
	another_user = models.ForeignKey(UserProfile)

	def __unicode__(self):		
		return '{0}; {1} - {2}'.format(self.pk, self.note.id, self.another_user.username)

