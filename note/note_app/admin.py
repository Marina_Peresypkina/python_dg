from django.contrib import admin
from note_app.models import Category, Font, Color, Tag, Note, Media, Permission


admin.site.register(Category)
admin.site.register(Font)
admin.site.register(Color)
admin.site.register(Tag)
admin.site.register(Note)
admin.site.register(Media)
admin.site.register(Permission)