from note_app.models import Category, Font, Color, Tag, Note, Media, Permission
from user_app.models import UserProfile

def get_color():
	return 	[color for color in Color.objects.all()]

def get_font():
	return 	[font for font in Font.objects.all()]

def get_tag():
	return 	[tag for tag in Tag.objects.all()]

def get_category():
	return 	[category for category in Category.objects.all()]

def get_user():
	return 	[category for category in Category.objects.all()]