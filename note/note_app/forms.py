from django import forms
from note_app.models import Category, Font, Color, Tag, Note, Media, Permission
from user_app.models import UserProfile
from note_app.actions import get_color, get_font, get_tag, get_category
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login


class AddNoteForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		self.user = kwargs.pop('user', None)
		print "vnutr" +str(self.user)
		super(AddNoteForm, self).__init__(*args, **kwargs)

	#category = forms.ChoiceField(choices=[(category,category) for category in Category.objects.filter(user = 4)])

	class Meta:
		model = Note
		fields = ['user', 'title', 'text', 'category','font', 'color', 'tag', 'permiss_all_users']

class EditNoteForm(forms.ModelForm):
	class Meta:
		model = Note 
		fields = ['title', 'text', 'category','font', 'color', 'tag', 'permiss_all_users']

class AddTagForm(forms.ModelForm):
	
	class Meta:
		model = Tag
		fields = ['name']

class AddCategoryForm(forms.ModelForm):
	
	class Meta:
		model = Category
		fields = ['name', 'parent']

class AddMediaForm(forms.ModelForm):
	
	class Meta:
		model = Media
		fields = ['note', 'media', 'file_type']

class AddPermissionForm(forms.ModelForm):

	class Meta:
		model = Permission
		fields = ['note', 'another_user']