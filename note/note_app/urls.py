from django.conf.urls import url
from django.conf import settings
from note_app import views
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    
	url(r'^$', views.NoteList.as_view(), name="main"),
	url(r'^main/$', views.NoteList.as_view(), name="main"),
	url(r'^mynotes/$', views.MyNoteList.as_view(), name="mynote_list"),
	url(r'^note/(?P<pk>\d+)/$', views.MyNote.as_view(), name="note"),
	url(r'^add_note/', views.AddNoteView.as_view(), name="add_note"),
	url(r'^edit_note/(?P<pk>\d+)/$', views.EditNoteView.as_view(), name="edit_note"),
	url(r'^add_tag/', views.AddTagView.as_view(), name="add_tag"),
	url(r'^add_category/', views.AddCategoryView.as_view(), name="add_category"),
	url(r'^add_media/', views.AddMediaView.as_view(), name="add_media"),
	url(r'^add_permission/', views.AddPermissionView.as_view(), name="add_permission"),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()