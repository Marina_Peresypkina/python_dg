"""config URL Configuration

"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
	url(r'^user/', include('user_app.urls')),
	url(r'^', include('note_app.urls')),
	url(r'^restframe/', include('restframe.urls')),
]
