from wsgiref.simple_server import make_server
from cgi import parse_qs
import urlparse
import random


html = """
<html>
<body>
	<form  method="POST" action="parsing_post.wsgi" >
		<p><input type="text" name="some"/></p>
		<p><input type="submit"></p>
	</form>
</body>
</html>
"""
html2 = """
<html>
<body>
%s
</body>
</html>
"""


class Url_Router:

	def _parse_environ(self, environ):
		return {'method':environ['REQUEST_METHOD']}
	
	def is_valid_url(self, url):
		res = urlparse.urlparse(url)
		return res.scheme in['http', 'https']
	
	def encode(self, url):
		urlencode[str(len(url))]=url
		
	def despatch(self, environ):
		r_d = self._parse_environ(environ)
		if r_d['method']=='GET':
			return html
		elif r_d['method']=='POST':
			s = int(environ.get('CONTENT_LENGTH', 0))
			d = environ['wsgi.input'].read(s)
			d = parse_qs(d)
			url = d['some'][0]
			if self.is_valid_url(url):
				str_len = str(len(url))
				return self.key_url(url)
			else:
				return html
			return html
			#			
	
	def key_url(self, url):
		dictionary = {}
		hash = random.getrandbits(128)
		while True:
			if hash not in dictionary.keys():
				dictionary[hash] = url
				break
			hash = random.getrandbits(128)
		return hash	

class Shortly:
	router = Url_Router()
	def __call__(self, environ, start_response):
		start_response('200 OK', [('Content-type','text/html')])
		return [self.router.despatch(environ)]


server = make_server('localhost', 8080, Shortly())
server.serve_forever()




