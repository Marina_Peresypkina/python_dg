from django.conf.urls import url
from user_app import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
	#url(r'^avtoriz/$', views.Avtorization.as_view(), name='avtoriz'),
	#url(r'^authorize/$', views.Avtorization.as_view(), name="authorize"),
	url(r'^register/$', views.Register.as_view(), name="register"),
	url(r'^avtorizapi/$', views.AvtorizationApi.as_view(), name='avtorizapi'),
	url(r'^logout/$', views.LogOut.as_view(), name='logout'),
	#url(r'^avtoriz/logout/$', views.LogOut.as_view(), name='logout'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()