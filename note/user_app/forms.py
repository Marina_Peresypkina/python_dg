# -*- coding: utf-8 -*-
from django import forms
from user_app.models import UserProfile
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from django.contrib.auth import authenticate, get_user_model
from django.utils.text import capfirst



class UserRegisterForm(forms.ModelForm):
	""" Registration form for user """

	username = forms.CharField(label='username')
	email = forms.CharField(label='Email', widget=forms.EmailInput, required=True)
	password = forms.CharField(label='Password', widget=forms.PasswordInput)
	password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

	class Meta:
		model = UserProfile
		fields = ['username', 'email','first_name', 'last_name', 'avatar', 'password']

	def clean_password2(self):
		password = self.cleaned_data.get("password")
		password2 = self.cleaned_data.get("password2")
		if password and password2 and password != password2:
			raise forms.ValidationError("Passwords don't match")
		return password2

	def save(self, commit=True):
		user = super(UserRegisterForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password"])
		if commit:
			user.save()
		return user

class UserAuthenticForm(forms.Form):
	error_messages = {
		'invalid_login': "Please enter a correct username and password. "
						   "Note that both fields may be case-sensitive.",
		'inactive': "This account is inactive.",
	}

	username = forms.CharField(max_length=254)
	password = forms.CharField(label="Password", widget=forms.PasswordInput)
