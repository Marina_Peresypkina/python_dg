# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_app', '0004_auto_20150620_1700'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='first_name',
            new_name='f_name',
        ),
        migrations.RenameField(
            model_name='userprofile',
            old_name='last_name',
            new_name='l_name',
        ),
    ]
