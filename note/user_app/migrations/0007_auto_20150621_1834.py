# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_app', '0006_auto_20150621_1832'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='f_name',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='l_name',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='nickname',
        ),
    ]
