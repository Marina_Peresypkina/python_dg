# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django .contrib.contenttypes.models import ContentType
from django.contrib.auth.models import AbstractUser
from datetime import datetime


class UserProfile(AbstractUser):

	avatar = models.ImageField(upload_to='images/user_avatar', blank=True, null=True)

	
	#USERNAME_FIELD = 'username'
	#REQUIRED_FIELDS = ['email']

	def __unicode__(self):
		return '{0} - {1}'.format(self.pk, self.username)