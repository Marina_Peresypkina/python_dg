from django.views.generic import TemplateView, DetailView, ListView, View 
from user_app.models import UserProfile
from user_app.forms import UserRegisterForm, UserAuthenticForm
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.edit import FormView, UpdateView  
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, user_logged_out
from django.conf import settings

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import authenticate, login
from rest_framework.generics import GenericAPIView
from user_app.models import UserProfile
from restframe.serializers import SerialUser
from rest_framework import authentication, serializers


from rest_framework import generics, permissions, status, response, views

class Register(FormView):
	"""Registration for user"""

	template_name = 'user_app/registr.html'
	success_url = '/main/'
	form_class = UserRegisterForm

	def form_valid(self, form):
		self.object = form.save()
		return super(Register, self).form_valid(form)


class AvtorizationApi(APIView):
	"""Authentication for user"""

	authentication_classes = (BasicAuthentication,)
	serializer_class = SerialUser

	def post(self, request, *args, **kwargs):

		user = authenticate(username=request.data.get("username"), password=request.data.get("password"))
		login(request, user)
		return HttpResponseRedirect('/main/')

	def delete(self, request, *args, **kwargs):
		"""Logout from system"""
		
		logout(request)
		return Response("ok")

class Avtorization(FormView):
	template_name = 'user_app/avtoriz.html'
	success_url = '/main/'
	form_class = UserAuthenticForm

	def form_valid(self, form):
		user = form.get_user()
		login(self.request, user)
		return super(Avtorization, self).form_valid(form)

class LogOut(View):
	"""Logout from system"""

	def get(self, request, *args, **kwargs):
		logout(request)
		return HttpResponseRedirect('/user/avtorizapi/')