from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from user_app.models import UserProfile
from note_app.models import Category, Font, Color, Tag, Note, Media, Permission
from restframe.serializers import SerialNote, UserProfile, SerialCategory, SerialPermission, SerialMedia, SerialColor, SerialTag, SerialCategoryNoUser, SerialMediaNoUser, SerialNoteNoUser,SerialNoteForEdid
from django.views.generic import TemplateView
from rest_framework.response import Response
from rest_framework import permissions, status, serializers
from itertools import chain

from rest_framework import viewsets 
from rest_framework.decorators import detail_route
from django.shortcuts import get_object_or_404



class NoteListApi(APIView):
	"""Get list of available for user notes"""

	permission_classes = (permissions.IsAuthenticated,)
	def get(self, request):

		id_n = Permission.objects.filter(another_user=request.user.pk)
		notes = Note.objects.filter(pk__in=id_n.values('note_id'))
		notes_for_all = Note.objects.filter( permiss_all_users=True)
		if notes_for_all:
			result_list = list(chain(notes, notes_for_all))
			note_list = SerialNote(result_list, many=True)
		else:
			note_list = SerialNote(notes, many=True)	
		return Response(note_list.data)

class MyNoteListApi(viewsets.ModelViewSet):
	"""Get list of user notes"""

	queryset = Note.objects.all()
	serializer_class = SerialNoteNoUser
	permission_classes = (permissions.IsAuthenticated,)

	def list(self, request):
		queryset = Note.objects.filter(user=request.user.pk)
		serializer = SerialNote(queryset, many=True)
		return Response(serializer.data)

	def create(self, request):
		request.data['user'] = request.user.pk		
		serializer = SerialNoteForEdid(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response( status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def retrieve(self, request, pk=None):
		queryset = Note.objects.filter(user = request.user.pk)
		get_note = get_object_or_404(queryset, pk=pk)
		serializer = SerialNote(get_note)
		return Response(serializer.data)

class TagViewSet(viewsets.ModelViewSet):
	"""
 	Returns a list of tags.
 	Edit, delete and add.
	"""	
	queryset = Tag.objects.all()
	serializer_class = SerialTag    
	permission_classes = (permissions.IsAuthenticated,)

class PermissionViewSet(viewsets.ModelViewSet):
	"""
 	Returns a list of permission for note.
 	Edit, delete and add.
	"""	
	queryset = Permission.objects.all()
	serializer_class = SerialPermission    
	permission_classes = (permissions.IsAuthenticated,)

	def list(self, request):
		queryset = Permission.objects.filter(note__user = request.user.pk)
		serializer = SerialPermission(queryset, many=True)
		return Response(serializer.data)
	
	def create(self, request):
		serializer = SerialPermission(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)	

	def retrieve(self, request, pk=None):
		queryset = Permission.objects.filter(note__user = request.user.pk)
		get_permiss = get_object_or_404(queryset, pk=pk)
		serializer = SerialPermission(get_permiss)
		return Response(serializer.data)

class CategoryViewSet(viewsets.ModelViewSet):
	"""
 	Returns a list of user categories.
 	Edit, delete and add.
	"""
	queryset = Category.objects.all()
	serializer_class = SerialCategoryNoUser
	permission_classes = (permissions.IsAuthenticated,)

	def list(self, request):
		queryset = Category.objects.filter(user = request.user.pk)
		serializer = SerialCategory(queryset, many=True)
		return Response(serializer.data)

	def create(self, request):
		request.data['user'] = request.user.pk
		serializer = SerialCategory(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)	

	def retrieve(self, request, pk=None):
		queryset = Category.objects.filter(user = request.user.pk)
		get_categ = get_object_or_404(queryset, pk=pk)
		serializer = SerialCategory(get_categ)
		return Response(serializer.data)	


class MediaViewSet(viewsets.ModelViewSet):
	"""
 	Returns a list of user medias.
 	Edit, delete and add.
	"""
	queryset = Media.objects.all()
	serializer_class = SerialMediaNoUser
	permission_classes = (permissions.IsAuthenticated,)

	def list(self, request):
		queryset = Media.objects.filter(user = request.user.pk)
		serializer = SerialMedia(queryset, many=True)
		return Response(serializer.data)

	def create(self, request):
		request.data['user'] = request.user.pk
		#note_user = Note.objects.filter(user=request.user.pk).values_list('pk', flat=True).distinct()
		serializer = SerialMedia(data=request.data)
		if serializer.is_valid():		
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)	
		# if serializer.is_valid():
		# 	for item in note_user:
		# 		if item == request.data['note']:		
		# 			serializer.save()
		# 			return Response(serializer.data, status=status.HTTP_201_CREATED)
		# return Response("not")

	def retrieve(self, request, pk=None):
		queryset = Media.objects.filter(user = request.user.pk)
		get_media = get_object_or_404(queryset, pk=pk)
		serializer = SerialMedia(get_media)
		return Response(serializer.data)	

