from django.conf.urls import url
from django.conf import settings
from restframe import views
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


list_category = views.CategoryViewSet.as_view({
	'get': 'list',
	'post':'create'
	})

category_detail = views.CategoryViewSet.as_view({
	'get': 'retrieve',
	'put': 'update',
	'patch': 'partial_update',
	'delete': 'destroy'
	})

list_tag = views.TagViewSet.as_view({
	'get': 'list',
	'post':'create'
	})

tag_detail = views.TagViewSet.as_view({
	'get': 'retrieve',
	'put': 'update',
	'patch': 'partial_update',
	'delete': 'destroy'
	})

list_permiss = views.PermissionViewSet.as_view({
	'get': 'list',
	'post':'create'
	})

permiss_detail = views.PermissionViewSet.as_view({
	'get': 'retrieve',
	'put': 'update',
	'patch': 'partial_update',
	'delete': 'destroy'
	})

list_note = views.MyNoteListApi.as_view({
	'get': 'list',
	'post':'create'
	})

note_detail = views.MyNoteListApi.as_view({
	'get': 'retrieve',
	'put': 'update',
	'patch': 'partial_update',
	'delete': 'destroy'
	})

list_media = views.MediaViewSet.as_view({
	'get': 'list',
	'post':'create'
	})

media_detail = views.MediaViewSet.as_view({
	'get': 'retrieve',
	'put': 'update',
	'patch': 'partial_update',
	'delete': 'destroy'
	})


urlpatterns = [
	url(r'^note_list_api/$', views.NoteListApi.as_view(), name="note_list_api"),
	url(r'^category/$', list_category),
	url(r'^category/(?P<pk>\d+)/$', category_detail),
	url(r'^tag/$', list_tag),
	url(r'^tag/(?P<pk>\d+)/$', tag_detail),
	url(r'^permission/$', list_permiss),
	url(r'^permission/(?P<pk>\d+)/$', permiss_detail),
	url(r'^mynote_list_api/$', list_note),
	url(r'^mynote_list_api/(?P<pk>\d+)/$', note_detail),
	url(r'^media/$', list_media),
	url(r'^media/(?P<pk>\d+)/$', media_detail),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()