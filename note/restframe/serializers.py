from rest_framework import routers, serializers, viewsets
from note_app.models import Category, Font, Color, Tag, Note, Media, Permission
from user_app.models import UserProfile


class SerialCategoryNoUser(serializers.ModelSerializer):

	class Meta:

		model = Category
		fields = ['pk', 'name', 'parent']

class SerialCategory(serializers.ModelSerializer):

	class Meta:

		model = Category
		fields = ['pk', 'user', 'name', 'parent']

class SerialColor(serializers.ModelSerializer):
	class Meta:
		model = Color
		fields = ['pk', 'name', 'color_code']

class SerialTag(serializers.ModelSerializer):
	class Meta:
		model = Tag
		fields = ['pk', 'name']

class SerialMediaNoUser(serializers.ModelSerializer):	
	class Meta:
		model = Media
		fields = ['pk','note', 'media', 'file_type']

class SerialMedia(serializers.ModelSerializer):	
	class Meta:
		model = Media
		fields = ['pk','note', 'user', 'media', 'file_type']

class SerialNoteNoUser(serializers.ModelSerializer):

	class Meta:
		model = Note
		fields = ['pk', 'title', 'text', 'category', 'color', 'font', 'tag', 'permiss_all_users']

class SerialNoteForEdid(serializers.ModelSerializer):

	class Meta:
		model = Note
		fields = ['pk','user', 'title', 'text', 'category', 'color', 'font', 'tag', 'permiss_all_users', 'medias']	

class SerialNote(serializers.ModelSerializer):

	user = serializers.ReadOnlyField(source='user_name')
	color = serializers.ReadOnlyField(source='color_code')
	font = serializers.ReadOnlyField(source='font_name')
	category = serializers.ReadOnlyField(source='category_name')
	tag = SerialTag(many=True)	
	medias = SerialMedia(many=True)

	class Meta:
		model = Note
		fields = ['pk','user', 'title', 'text', 'category', 'color', 'font', 'tag', 'permiss_all_users', 'medias']
	
class SerialPermission(serializers.ModelSerializer):
	class Meta:
		model = Permission
		fields = ['pk','note', 'another_user']	

class SerialUser(serializers.ModelSerializer):
	class Meta:
		model = UserProfile
		fields = ['pk', 'username', 'password']	

